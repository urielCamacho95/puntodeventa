<?php
		session_start();
		include('php_conexion.php'); 
		if(!$_SESSION['tipo_usu']=='a' or !$_SESSION['tipo_usu']=='ca'){
			header('location:error.php');
		}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Inventario</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link href="css/docs.css" rel="stylesheet">
    <link href="js/google-code-prettify/prettify.css" rel="stylesheet">
	
	<link href="css/estilos.css" rel="stylesheet">
	 <link href="css/fuentes.css" rel="stylesheet">
	 
    <script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
	<script src="js/jquery.js"></script>
    <script src="js/bootstrap-transition.js"></script>
    <script src="js/bootstrap-alert.js"></script>
    <script src="js/bootstrap-modal.js"></script>
    <script src="js/bootstrap-dropdown.js"></script>
    <script src="js/bootstrap-scrollspy.js"></script>
    <script src="js/bootstrap-tab.js"></script>
    <script src="js/bootstrap-tooltip.js"></script>
    <script src="js/bootstrap-popover.js"></script>
    <script src="js/bootstrap-button.js"></script>
    <script src="js/bootstrap-collapse.js"></script>
    <script src="js/bootstrap-carousel.js"></script>
    <script src="js/bootstrap-typeahead.js"></script>
    <script src="js/bootstrap-affix.js"></script>
    <script src="js/holder/holder.js"></script>
    <script src="js/google-code-prettify/prettify.js"></script>
    <script src="js/application.js"></script>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="assets/ico/favicon.png">

</head>
<body data-spy="scroll" data-target=".bs-docs-sidebar">
<div align="center">
<table width="80%" border="0" class="table tablas">
  <tr class="info">
    <td colspan="4"><center><strong>Creacion de Productos/Actualización de Productos</strong></center></td>
  </tr>
   <tr>
    <td colspan="3">
    <div class="control-group info">
    <form name="form1" method="post" action="">
    	<div class="input-append">
   			 <input class="span2" id="ccodigo" name="ccodigo" type="text" placeholder="Codigo del Articulo">
    	 	 <button class="btn botones" type="submit">Confirmar Codigo</button>
   		</div>
    </form>
    </div>
    <?php 
		
		if(!empty($_POST['ccodigo']) or !empty($_GET['codigo'])){	
			$prov='';$nom='';$costo='0';$mayor='0';$cantidad='0';$minimo='0';$seccion='';$codigo='';
			$venta='0';$cprov='';
			$fechax=date("d").'/'.date("m").'/'.date("Y");
			$fechay=date("Y-m-d");
			if(!empty($_GET['codigo'])){
				$codigo=$_GET['codigo'];
			}
			if(!empty($_POST['ccodigo'])){
				$codigo=$_POST['ccodigo'];
			}
			$can=mysqli_query($link,"SELECT * FROM producto where cod='$codigo'");
			if($dato=$can->fetch_array(MYSQLI_ASSOC)){
				$nom=$dato['nom'];
				$costo=$dato['costo'];
				$venta=$dato['venta'];
				$cantidad=$dato['cantidad'];
				$fechay=$dato['fecha'];
				$boton="Actualizar Producto";
				echo '	<div class="alert alert-success">
						  <button type="button" class="close" data-dismiss="alert">X</button>
						  <strong>Producto / Articulo '.$nom.' </strong> con el codigo '.$codigo.' ya existe
					</div>';	
			}else{
				$boton="Guardar Producto";
			}
	?>
    </td>    
    <div class="control-group info">
    <form name="form2" method="post" enctype="multipart/form-data" action="">
  	<tr>
		<td width="8%"></td>
    	<td width="30%">
        	<label>Codigo: </label><input type="text" name="codigo" id="codigo" value="<?php echo $codigo; ?>" required>
            <label>Nombre: </label><input type="text" name="nom" id="nom" value="<?php echo $nom; ?>" required>
            <label>Fecha: </label><input type="date" name="fecha" id="fecha" value="<?php echo $fechay; ?>" required>
            <label>Precio Costo</label>
            <div class="input-prepend input-append">
                <span class="add-on">$</span>
                <input type="text" name="costo" id="costo" value="<?php echo $costo; ?>" required> 
                <span class="add-on">.00</span>
            </div>
            
        </td>
    	<td width="28%">
            <label>Cantidad Actual: </label><input type="text" name="cantidad" id="cantidad" value="<?php echo $cantidad; ?>" required>
            <label>Precio Venta: </label>
            <div class="input-prepend input-append">
                <span class="add-on">$</span>
                <input type="text" name="venta" id="venta" value="<?php echo $venta; ?>" required> 
                <span class="add-on">.00</span>
            </div><br><br>
           
            	<button type="submit" class="btn btn-primary botones"><?php echo $boton; ?></button>
           
        </td>
    	<td width="48%">
       		<center><label><strong>Imagen del Producto</strong></label></center>
            <center>
			<?php
				if (file_exists("articulo/".$codigo.".jpg")){
					echo '<img src="articulo/'.$codigo.'.jpg" width="200" height="200" class="img-polaroid">';
				}else{ 
					echo '<img src="articulo/producto.png" width="200" height="200" class="img-polaroid">';
				}
			?>
            </center><br>
            <center><input type="file" name="imagen" id="imagen"></center>
        </td>      
	</tr>
    </form>
    </div>
	<?php } ?>  
  </table>
   <?php 
		if(!empty($_POST['nom'])){
			$gnom=$_POST['nom'];		$gcosto=$_POST['costo'];
		$gventa=$_POST['venta'];		$gcantidad=$_POST['cantidad'];
			$gfecha=$_POST['fecha'];
			$gcodigo=$_POST['codigo'];
			
			$can=mysqli_query($link,"SELECT * FROM producto where cod='$gcodigo'");
			if($dato=$can->fetch_array(MYSQLI_ASSOC)){
				$sql="Update producto Set  	nom='$gnom',
											costo='$gcosto',
											venta='$gventa',
											cantidad='$gcantidad',
											fecha='$gfecha'			
							Where cod='$gcodigo'";
				mysqli_query($link,$sql);
				echo '	<div class="alert alert-success">
						  <button type="button" class="close" data-dismiss="alert">X</button>
						  <strong>Producto / Articulo '.$gnom.' </strong> Actualizado con Exito
					</div>';
				$prov='';$nom='';$costo='0';$mayor='0';$cantidad='0';$minimo='0';$seccion='';$fecha='';$codigo='';$venta='0';$cprov='';
			}else{
				$sql = "INSERT INTO producto (cod, nom, costo, venta, cantidad, fecha) 
							 VALUES ($gcodigo,'$gnom','$gcosto','$gventa','$gcantidad','$gfecha')";
				mysqli_query($link,$sql);	
				echo '	<div class="alert alert-success">
						  <button type="button" class="close" data-dismiss="alert">X</button>
						  <strong>Producto / Articulo '.$gnom.' </strong> Guardado con Exito
						</div>';
			}
			//subir la imagen del articulo
			$nameimagen = $_FILES['imagen']['name'];
			$tmpimagen = $_FILES['imagen']['tmp_name'];
			$extimagen = pathinfo($nameimagen);
			$ext = array("png","jpg");
			$urlnueva = "articulo/".$gcodigo.".jpg";			
			if(is_uploaded_file($tmpimagen)){
				if(array_search($extimagen['extension'],$ext)){
					copy($tmpimagen,$urlnueva);	
				}
			}
		}
		?>
</div>
</body>
</html>