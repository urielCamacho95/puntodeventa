<?php
 		session_start();
		include('php_conexion.php'); 
		
		if(!$_SESSION['tipo_usu']=='a'){
			header('location:error.php');
		}
		$usuario=$_SESSION['username'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Blanco</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link href="css/docs.css" rel="stylesheet">
    <link href="js/google-code-prettify/prettify.css" rel="stylesheet">
    <script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
	<script src="js/jquery.js"></script>
    <script src="js/bootstrap-transition.js"></script>
    <script src="js/bootstrap-alert.js"></script>
    <script src="js/bootstrap-modal.js"></script>
    <script src="js/bootstrap-dropdown.js"></script>
    <script src="js/bootstrap-scrollspy.js"></script>
    <script src="js/bootstrap-tab.js"></script>
    <script src="js/bootstrap-tooltip.js"></script>
    <script src="js/bootstrap-popover.js"></script>
    <script src="js/bootstrap-button.js"></script>
    <script src="js/bootstrap-collapse.js"></script>
    <script src="js/bootstrap-carousel.js"></script>
    <script src="js/bootstrap-typeahead.js"></script>
    <script src="js/bootstrap-affix.js"></script>
    <script src="js/holder/holder.js"></script>
    <script src="js/google-code-prettify/prettify.js"></script>
    <script src="js/application.js"></script>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="assets/ico/favicon.png">
	
	<link href="css/estilos.css" rel="stylesheet">
	 <link href="css/fuentes.css" rel="stylesheet">

</head>
<body data-spy="scroll" data-target=".bs-docs-sidebar">
<table width="70%" border="0" class="table tablas" align="center">
  <tr class="info">
    <td><strong>Cambiar Contraseña</strong></td>
  </tr>
  <tr>
	<td></td>
	<td></td>
    <td width="50%">
    <form name="form1" method="post" action="">
      <label>Contraseña Antigua: </label><input type="password" name="contra" id="contra">
      <label>Nueva Contraseña: </label><input type="password" name="c1" id="c1" required>
      <label>Repita Nueva Contraseña: </label><input type="password" name="c2" id="c2" required><br><br>
      <input type="submit" name="button" id="button" class="btn btn-primary botones" value="Cambiar Contraseña">
    </form>
    </td>
    <td width="50%">
    <br><br><br><br><br><br>
    <?php 
	if(!empty($_POST['c1']) and !empty($_POST['c2']) and !empty($_POST['contra'])){
		if($_POST['c1']==$_POST['c2']){
			$contra=$_POST['contra'];
			$can=mysqli_query($link,"SELECT * FROM usuarios WHERE usu='".$usuario."' and con='".$contra."'");
			if($dato=$can ->fetch_array(MYSQLI_ASSOC)){
				$cnueva=$_POST['c2'];
				$sql="Update usuarios Set con='$cnueva' Where usu='$usuario'";
				mysqli_query($link,$sql);
				echo '<div class="alert alert-success">
					  <button type="button" class="close" data-dismiss="alert">×</button>
					  <strong>Contraseña!</strong> Actualizada con exito
					</div>';
			}else{
				echo '<div class="alert alert-error">
					  <button type="button" class="close" data-dismiss="alert">×</button>
					  <strong>Contraseña!</strong> Digitada no corresponde a la antigua
					</div>';
			}
		}else{
			echo '<div class="alert alert-error">
					  <button type="button" class="close" data-dismiss="alert">×</button>
					  <strong>Las dos Contraseña!</strong> Digitadas no soy iguales
					</div>';
		}
	}
	?>
    </td>
  </tr>
</table>
</body>
</html>