<?php
 		session_start();
		include('php_conexion.php'); 
		if(!$_SESSION['tipo_usu']=='a' or !$_SESSION['tipo_usu']=='ca'){
			header('location:error.php');
		}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Datos usuario</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link href="css/docs.css" rel="stylesheet">
    <link href="js/google-code-prettify/prettify.css" rel="stylesheet">
    <script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
	<script src="js/jquery.js"></script>
    <script src="js/bootstrap-transition.js"></script>
    <script src="js/bootstrap-alert.js"></script>
    <script src="js/bootstrap-modal.js"></script>
    <script src="js/bootstrap-dropdown.js"></script>
    <script src="js/bootstrap-scrollspy.js"></script>
    <script src="js/bootstrap-tab.js"></script>
    <script src="js/bootstrap-tooltip.js"></script>
    <script src="js/bootstrap-popover.js"></script>
    <script src="js/bootstrap-button.js"></script>
    <script src="js/bootstrap-collapse.js"></script>
    <script src="js/bootstrap-carousel.js"></script>
    <script src="js/bootstrap-typeahead.js"></script>
    <script src="js/bootstrap-affix.js"></script>
    <script src="js/holder/holder.js"></script>
    <script src="js/google-code-prettify/prettify.js"></script>
    <script src="js/application.js"></script>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="assets/ico/favicon.png">
	
	<link href="css/estilos.css" rel="stylesheet">
	 <link href="css/fuentes.css" rel="stylesheet">

</head>
<body data-spy="scroll" data-target=".bs-docs-sidebar">
<center>
<div class="btn-group " data-toggle="buttons-checkbox" align="center">
        <button type="button" class="btn btn-primary botones" onClick="window.location='agrega_usuario.php'">Agregar un usuario</button>
</div></center><br><br>
<div align="right">
    <!-- Button to trigger modal -->
<table width="80%" border="0" class="table table-hover tablas" align="center">
<thead>
  <tr>
    <td colspan="6"><strong><center>Lista de usuarios</center></strong></td>
    </tr>
  <tr>
	<td width="25%"></td>
    <td width="10%"><strong>Usuario</strong></td>
    <td width="13%"><strong>Nombre</strong></div></td>
    <td width="26%"><strong>Tipo</strong></td>
  </tr>
</thead>
<tbody>
<?php 
	$mensaje='no';
    $can=mysqli_query($link,"SELECT * FROM usuarios"); 
    while($dato=$can ->fetch_array(MYSQLI_ASSOC)){
		$nom=$dato['nom'];
		$usu=$dato['usu'];
    $tipo=$dato['tipo'];
?>

  <tr>
    <form action="borra_usuario.php" method="get">
    <?php $variable = $dato['usu']; ?>
	<td></td>
    <td><input type="hidden" name="us" value="<?php echo $variable ?>"/><?php echo $dato['usu']; ?></td>
    <td><a href="modifica_usuario.php?codigo=<?php echo $dato['usu']; ?>"><?php echo $dato['nom']; ?> </a></td>
    <?php if ($dato['tipo']=='a') {
    ?>
    <td><div align="left"><?php echo "Administrador"; }?></div></td>
    <?php if ($dato['tipo']=='ca') {
    ?>
    <td><div align="left"><?php echo "Cajero"; }?></div></td>
    <td><input type="submit" class="btn btn-primary botones" value="Eliminar usuario"/></td>
    </form>
  </tr>
<?php } ?>
  </tbody>
</table>
<br><br>
</body>
</html>